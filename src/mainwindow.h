#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDialog>
#include <QStringList>
#include <QStringListModel>
#include <QAbstractItemView>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_enableRoot_clicked();

    void on_resetForRoot_clicked();

    void on_sshServerInstall_clicked();

    void on_sshClientInstall_clicked();

    void on_downloadJDK_clicked();

    void on_installJDK_clicked();

    void on_downloadHadoop_clicked();

    void on_installHadoop_clicked();

    void on_resetComputer_clicked();

    void on_addItem_clicked();

    void on_removeItem_clicked();

    void on_setConfig_clicked();

    void on_publicKeyGen_clicked();

private:
    Ui::MainWindow *ui;
    QStringListModel *model;
};

#endif // MAINWINDOW_H
