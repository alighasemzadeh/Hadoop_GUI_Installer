/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *enableRoot;
    QLabel *label;
    QLineEdit *passwordText;
    QLabel *label_2;
    QPushButton *resetForRoot;
    QLabel *label_3;
    QPushButton *sshServerInstall;
    QPushButton *sshClientInstall;
    QLabel *label_4;
    QPushButton *downloadJDK;
    QPushButton *installJDK;
    QLabel *label_5;
    QPushButton *downloadHadoop;
    QPushButton *installHadoop;
    QPushButton *resetComputer;
    QPushButton *publicKeyGen;
    QPushButton *removeItem;
    QLabel *label_8;
    QPushButton *addItem;
    QPushButton *setConfig;
    QLabel *label_7;
    QListView *nodeList;
    QLabel *label_6;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(842, 425);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        enableRoot = new QPushButton(centralWidget);
        enableRoot->setObjectName(QStringLiteral("enableRoot"));
        enableRoot->setGeometry(QRect(210, 40, 151, 27));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 251, 17));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        passwordText = new QLineEdit(centralWidget);
        passwordText->setObjectName(QStringLiteral("passwordText"));
        passwordText->setGeometry(QRect(90, 40, 113, 27));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 40, 71, 17));
        resetForRoot = new QPushButton(centralWidget);
        resetForRoot->setObjectName(QStringLiteral("resetForRoot"));
        resetForRoot->setGeometry(QRect(370, 40, 98, 27));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 80, 251, 17));
        label_3->setFont(font);
        sshServerInstall = new QPushButton(centralWidget);
        sshServerInstall->setObjectName(QStringLiteral("sshServerInstall"));
        sshServerInstall->setGeometry(QRect(80, 100, 161, 27));
        sshClientInstall = new QPushButton(centralWidget);
        sshClientInstall->setObjectName(QStringLiteral("sshClientInstall"));
        sshClientInstall->setGeometry(QRect(250, 100, 161, 27));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 130, 251, 17));
        label_4->setFont(font);
        downloadJDK = new QPushButton(centralWidget);
        downloadJDK->setObjectName(QStringLiteral("downloadJDK"));
        downloadJDK->setGeometry(QRect(80, 150, 161, 27));
        installJDK = new QPushButton(centralWidget);
        installJDK->setObjectName(QStringLiteral("installJDK"));
        installJDK->setGeometry(QRect(250, 150, 161, 27));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 180, 251, 17));
        label_5->setFont(font);
        downloadHadoop = new QPushButton(centralWidget);
        downloadHadoop->setObjectName(QStringLiteral("downloadHadoop"));
        downloadHadoop->setGeometry(QRect(80, 200, 161, 27));
        installHadoop = new QPushButton(centralWidget);
        installHadoop->setObjectName(QStringLiteral("installHadoop"));
        installHadoop->setGeometry(QRect(250, 200, 161, 27));
        resetComputer = new QPushButton(centralWidget);
        resetComputer->setObjectName(QStringLiteral("resetComputer"));
        resetComputer->setGeometry(QRect(720, 300, 98, 27));
        publicKeyGen = new QPushButton(centralWidget);
        publicKeyGen->setObjectName(QStringLiteral("publicKeyGen"));
        publicKeyGen->setGeometry(QRect(560, 330, 171, 27));
        removeItem = new QPushButton(centralWidget);
        removeItem->setObjectName(QStringLiteral("removeItem"));
        removeItem->setGeometry(QRect(480, 260, 71, 27));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(480, 230, 321, 17));
        addItem = new QPushButton(centralWidget);
        addItem->setObjectName(QStringLiteral("addItem"));
        addItem->setGeometry(QRect(750, 260, 71, 27));
        setConfig = new QPushButton(centralWidget);
        setConfig->setObjectName(QStringLiteral("setConfig"));
        setConfig->setGeometry(QRect(480, 300, 98, 27));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(480, 20, 251, 17));
        label_7->setFont(font);
        nodeList = new QListView(centralWidget);
        nodeList->setObjectName(QStringLiteral("nodeList"));
        nodeList->setGeometry(QRect(480, 40, 341, 192));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(70, 250, 331, 91));
        QFont font1;
        font1.setPointSize(15);
        font1.setBold(true);
        font1.setWeight(75);
        label_6->setFont(font1);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 842, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Hadoop Installer", 0));
        enableRoot->setText(QApplication::translate("MainWindow", "Enable Root User", 0));
        label->setText(QApplication::translate("MainWindow", "Step 1:Enable Root Account", 0));
        passwordText->setInputMask(QString());
        label_2->setText(QApplication::translate("MainWindow", "Password:", 0));
        resetForRoot->setText(QApplication::translate("MainWindow", "Reset", 0));
        label_3->setText(QApplication::translate("MainWindow", "Step 2:Enable SSH", 0));
        sshServerInstall->setText(QApplication::translate("MainWindow", "Install Server SSH", 0));
        sshClientInstall->setText(QApplication::translate("MainWindow", "Install Client SSH", 0));
        label_4->setText(QApplication::translate("MainWindow", "Step 3:JDK Setup", 0));
        downloadJDK->setText(QApplication::translate("MainWindow", "Download JDK", 0));
        installJDK->setText(QApplication::translate("MainWindow", "Install JDK", 0));
        label_5->setText(QApplication::translate("MainWindow", "Step 4:Hadoop Setup", 0));
        downloadHadoop->setText(QApplication::translate("MainWindow", "Download Hadoop", 0));
        installHadoop->setText(QApplication::translate("MainWindow", "Install Hadoop", 0));
        resetComputer->setText(QApplication::translate("MainWindow", "Reset", 0));
        publicKeyGen->setText(QApplication::translate("MainWindow", "Public Key Genaration", 0));
        removeItem->setText(QApplication::translate("MainWindow", "Remove", 0));
        label_8->setText(QApplication::translate("MainWindow", "Node Struct Example:master,192.168.1.1", 0));
        addItem->setText(QApplication::translate("MainWindow", "Add", 0));
        setConfig->setText(QApplication::translate("MainWindow", "Config", 0));
        label_7->setText(QApplication::translate("MainWindow", "Step 5:Configuration", 0));
        label_6->setText(QApplication::translate("MainWindow", "Azad University of Safashahr", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
